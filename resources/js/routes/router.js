import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// components

import Starter from '../components/StarterComponent.vue'
import Dashboard from '../components/DashboardComponent.vue'
import Settings from '../components/SettingsComponent.vue'
import Profile from '../components/settings/ProfileComponent.vue'
import GeneralSettings from '../components/settings/GeneralSettingsComponent.vue'
import SettingsInfo from '../components/settings/SettingsInfo.vue'
import Users from '../components/UsersComponent.vue'
import RolePermissionList from '../components/RolePermissionList.vue'

// error pages
import FourOhOne from  '../components/errors/401.vue'

// Auth
import Login from '../components/auth/Login.vue';
import Register from '../components/auth/Register.vue';

// define routes

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Starter',
            component: Starter,
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
        },
        {
            path: '/home',
            name: 'Dashboard',
            component: Dashboard,
        },
        {
            path: '/users',
            name: 'Users',
            component: Users
        },
        {
            path: '/role-permissions',
            name: 'RolePermissions',
            component: RolePermissionList
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Settings,
            children: [
                {
                    path: '/',
                    name: 'SettingsInfo',
                    component: SettingsInfo
                },
                {
                    path: 'general',
                    name: "General",
                    component: GeneralSettings
                },
                {
                    path: '/profile',
                    name: "Profile",
                    component: Profile
                }
            ]
        },

        {
            path: '/401',
            name: '401',
            component: FourOhOne,
        }
    ]
})

export default router