@extends('layouts.app')

@section('content')
<div id="app" class="wrapper">
        
    <welcome-component  :appname="'{{ config('app.name') }}'"></welcome-component>

</div>
@endsection
