@extends('layouts.app')

@section('content')
<div id="app" class="wrapper">
        
    <home-component :user="{{ Auth::user() }}" :appname="'{{ config('app.name') }}'"></home-component>

</div>
@endsection
