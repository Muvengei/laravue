<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/{path}', function () {
    return view('welcome');
})->where('path', '|login|register')->name('welcome');

Auth::routes();

Route::get('/{any}', 'HomeController@index')->name('home')->where('any', '401|home|users|role-permissions|settings.*');

Route::get('user', function () {return response(Auth::user(), 200);});

Route::get('getUsers', "HomeController@getUsers");

Route::get('getRoles', "HomeController@getRoles");

Route::get('getUserRoles/{user}', "HomeController@getUserRoles");

Route::get('getPermissions', "HomeController@getPermissions");

Route::group(['middleware' => 'admin'], function() {
    Route::put('editUser/{user}', "HomeController@editUser");
    Route::put('confirmAccount/{user}', "HomeController@confirmAccount");
    Route::delete('deleteUser/{user}', "HomeController@deleteUser");
    Route::put('removeRole/{user}', "HomeController@removeRole");
    Route::put('removePermission/{role}', "HomeController@removePermission");
    Route::post('createRole', "HomeController@createRole");
    Route::put('updateRole/{role}', "HomeController@updateRole");
    Route::post('asignPermissionToRole', "HomeController@asignPermissionToRole");
    Route::delete('deleteRole/{role}', "HomeController@deleteRole");
    Route::post('createPermission', "HomeController@createPermission");
});

