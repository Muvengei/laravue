<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->hasRole('admin')) {
            # code...
            return $next($request);
        }else{
            $res = [
                'message' => "Unauthorized",
                'statusCode' => 401,
            ];
            return response($res, 401);
        }
        
    }
}
