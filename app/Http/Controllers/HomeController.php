<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function getUsers()
    {
        # code...
         $users = User::with('roles')->get();
        return response($users, 200);
    }

    public function getUserRoles($id)
    {
        # code...
        $user = User::find($id);
        $roles = $user->roles;
        return response($roles, 200);
    }

    public function getRoles()
    {
        # code...
        $roles = Role::with('permissions')->get();
        return response($roles, 200);
    }

    public function editUser($id, Request $request)
    {
        # code...
        $request->validate([
            'name'  => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->id
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        if ($request->role != null) {
            # code...
            $role = Role::findById($request->role);

            if ($role != null) {
                # code...
                $user->assignRole($role);
            }
        }
        return response($user, 200);

    }

    public function confirmAccount($id)
    {
        # code...
        $user = User::find($id);
        $confirmed = ($user->confirmed == true) ? false : true ;
        $user->confirmed = $confirmed;
        $user->save();
        return response($user, 200);
    }

    public function deleteUser($id)
    {
        # code...
        $user = User::find($id);

        $user->delete();

        return response('success', 200);
        
    }

    public function removeRole($id, Request $request)
    {
        # code...
        $user = User::find($id);
        $role = Role::findById($request->id);

        if ($user->removeRole($role)) {
            # code...
            return response($role, 200);
        }else{
            return response($user, 301);

        }

    }

    public function removePermission($id, Request $request)
    {
        # code...
        $role = Role::findById($id);

        $permission = Permission::findById($request->id);

        $role->revokePermissionTo($permission);

        return response('success', 200);
    }

    public function createRole(Request $request)
    {
        # code...
        $request->validate([
            'name'  => 'required|string|max:255|unique:roles'
        ]);

        $role = Role::create(['name' => $request->name]);
        // $role->givePermissionTo($request->selectedpermissions);

        return response()->json($role, 200);
    }

    public function updateRole($id, Request $request)
    {
        $request->validate([
            'name'  => 'required|string|max:255|unique:roles,name,'.$request->id
        ]);

        $role = Role::findById($request->id); 
        $role->update(['name' => $request->name]);
        $role->syncPermissions($request->selectedpermissions);

        return response()->json($role, 200);
    }

     public function deleteRole($id)
    {
        $role = Role::findById($id);
        $role->syncPermissions();
        $role->delete();

        return response()->json($role, 200);
    }


    public function asignPermissionToRole(Request $request)
    {
        # code...
        $role = Role::findById($request->id);
        $permission = Permission::findById($request->permission);
        if($role->givePermissionTo($permission->name)) {
            return response($role, 200);
        }

        return response('error', 404);
    }
    // PERMISSION
    public function getPermissions()
    {
        return Permission::latest()->get();
    }
    
    public function createPermission(Request $request)
    {
        $request->validate([
            'name'  => 'required|string|max:255|unique:permissions'
        ]);

        $permission = Permission::create(['name' => $request->name]);
        $permission->assignRole($request->selectedroles);

        return response()->json($permission, 200);
    }

}
