**A laravel vuejs starter template**

This is a laravel viuejs starter template with basic authentication, spatie permissions and a simple dashboard. 

---

## Getting started

Simple steps to get started.

1. Run **git clone https://bitbucket.org/Muvengei/laravue.git**
2. CD into **laravue**
3. Run **composer install**
4. Run **npm install**
5. Run **php artisan serve** and then open **localhost:8000** on your favourite browser
6. Code more modules into the repo

---

## Any issues, address to (geraldmuvengei06@gmail.com)

---

## Features

Here are already working features

1. Laravel basic authentication.
2. Spatie Role Permission management.
3. 

